## Provider
## Defining where to create resources and how to authenticate yourself
provider "aws" {
    region = "eu-west-1"
    # access_key = "my_access_key" ##this where these would be, but we don't need them as we're using roles
    # secret_key = "my_secret_key"
}

## Resources 
## Resources map to abstract/physical things in a cloud provider
## e.g. ec2 (physical) or a SG(abstract)
## each provider has it's library of resources. Use documentation
## https://registry.terraform.io/providers/hashicorp/aws/latest/docs


## Defining Variables

variable "identity"{
    description = "this is cohort and name of person"
    default = "cohort7-ramanavar"
}

variable "vpc-default"{
    description = "this is cohort and name of person"
    default = "vpc-4bb64132"
}


## Resource aws_instance ec2
## 
resource "aws_instance" "web"{
    ami = "ami-0635ca8333e27394d"
    instance_type = "t2.micro"
    subnet_id = "subnet-953a58cf"
    associate_public_ip_address = true
    security_groups = ["sg-01f897dc42d10c7de", "sg-091c83d3ecee7301e", aws_security_group.sg-web.id]
    tags = {
        "Name" = "${var.identity}-terraform"
    }
}

## Resource aws_security_group

resource "aws_security_group" "sg-web"{
    name = "sc-${var.identity}-terraform"
    description = "allows port 80 and 442 into instance"
    vpc_id = var.vpc-default
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "allowed port 80 from all"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "allowed port 443 from all"
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["185.59.124.94/32"]
        description = "allowed port 22 on ramana ip"
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
        description = "allow all outbound"
    }

    tags = {
        "Name" = "sc-ramana-terraform-test"
    }

}