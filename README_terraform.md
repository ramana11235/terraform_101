# Terraform Documentation

## Introduction 

Terraform is a software which provides setting up Infrastructure as Code (IAC) (when you dclaratively create infrastructure (servers, databases, etc.) from code). Cloud providers (aws, and others) take vare of of the rest as they provide (Infrastructure as a Service (IaaS)).

As part of infrastructure as code, there is configuration management and orchestration tools.

### Configuration Management

This is the process of setting up and configuring individual servers to a desired state. And also managing their mutations (any updates/changes performed thereafter).

Examples of configuration management tools:
- Chef
- Ansible
- Puppet
- Chef 

### Orchestration Tools 

These tools manage the networking and actual infrastructure (size of machines, subnets, VPCs, SG, IGW, Routes, etc.) we've set up. These tools also deploy instances into our infrastructure from AMI's (possibly set up by packer)

- 
- Cloudformation (AWS Terrafrom)
  
## Terraform Objective

The objective with Terraform is to build VPCs, and launch AMIs (including running init scripts and start services)

Examples of Terraform Usage:
- Create VPC
- Setup SG, NACL, Subnets
- Deploys AMI's into respective subnets
- Associate SG to instances
- Runs init scripts


Example Usage in DevOps:
- Origin code exists in BitBucket/Github
- Automation gets triggered from change in source
- CI pulls code into Jenkins and run tests
- Passing tests trigger next job in pipeline (CD)
- Jenkins uses Ansible to change machine to have new code
- Jenkins uses Packer to deliver an AMI 
- Jenkins uses Terraform to deploy AMI into production (CD)

## Terraform Main Commands
Main Commands:
- `terraform init`
  - To do first initialise of TF, downloads necessary modules
- `terraform plan `
  - Checks what you added in your .tf and shows you what your instances values would be
- `terraform apply` 
  - to apply those changes 
- `terraform destroy` 
  - destroys the instace

## Terraform Main Sections 
- variables
- resources
- provides


## Installing Terraform 

## Giving Permissions
If we're running terraform from our machine (not through AWS), we need to give terraform AWS secret and access keys.

Most clients will manage their keys differently depending of the host of the level of DevSecOps sophistication.

### 🚓🚓🚓🚨🚨🚨 **DO NOT** 🚨🚨🚨🚓🚓🚓 OPEN UP ITS THE FEDS
- Hard code your keys with code.
- Have them on a file which is ignored by .gitignore

### Possible Key Management Ways
- Setting them as environment variables for instances and interpolate (low level of sophistication)
- AIM roles (if infrastructure in cloud)
- Encrypted local vaults (e.g. ansible vault)
- Cloud Vaults (Hasicorp, Key Management Service (KMS from AWS))

## Examples and Code 

### "Resource" Syntax

```terraform
resource "specific_instance" "name_of_instance_in_tf"{
    param_resource = "value"
    other_param = "value"
}
```
- `specific_instance` is a resource within the library of the providers
- `name_of_instance_in_tf` is to specify the instance as defined in terraform
- `param_resource` are the specific parameters that can be defined for that resource
  - An ec2 might require an ami id 
  - A VPC might require an ip range 
  - Check Docs